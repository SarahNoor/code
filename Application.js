var myApp = angular.module('myApp', []);

myApp.controller('myController',function ($scope) {
    $scope.name = 'Sara';
    $scope.age =16;

    $scope.products = [
        {name:'cake 1', price: 1200},
        {name:'cake 2', price: 1000}
    ];

    $scope.cart = [];
    $scope.total = 0;

    $scope.addToCart = function(item) {
        $scope.total = $scope.total + item.price;
        $scope.cart.push(item);

    }
});
